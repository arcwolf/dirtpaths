package plugin.arcwolf.dirtpaths;

public class GlobalBlocks {

    private int blockId, blockData;

    public GlobalBlocks(int blockId, int blockData) {
        this.blockData = blockData;
        this.blockId = blockId;
    }

    /**
     * @return the blockId
     */
    public int getBlockId() {
        return blockId;
    }

    /**
     * @return the blockData
     */
    public int getBlockData() {
        return blockData;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + blockData;
        result = prime * result + blockId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        GlobalBlocks other = (GlobalBlocks) obj;
        if (blockData != other.blockData) return false;
        if (blockId != other.blockId) return false;
        return true;
    }
}
