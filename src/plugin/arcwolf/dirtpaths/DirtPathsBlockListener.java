package plugin.arcwolf.dirtpaths;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class DirtPathsBlockListener implements Listener {

    private DirtPaths plugin;

    public DirtPathsBlockListener(DirtPaths plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockSpread(BlockSpreadEvent event) {
        if (event.isCancelled()) return;
        Block block = event.getBlock();
        if (block.getType().equals(Material.DIRT) && block.getData() == 1) { // dirt was placed by player.
            if (plugin.debug == 2) DirtPaths.LOGGER.info(plugin.getName() + ": Blocked Grass Spread at " + event.getBlock().getLocation());
            event.setCancelled(true); // Canceled grass spread to this dirt.
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.isCancelled()) return;
        Player player = event.getPlayer();
        DirtPathSettings settings = DirtPathSettings.getSettings(player);
        if (!event.isCancelled() && event.getBlock().getType() == Material.DIRT && settings.command.equals("dpath")) {
            if (plugin.debug == 2) DirtPaths.LOGGER.info(plugin.getName() + ": Set Dirt/Grass spread prevention databit at " + event.getBlock().getLocation());
            event.getBlock().setData((byte) 1); // set built flag
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockForm(BlockFormEvent event) {
        if (event.isCancelled()) return;
        Block snow = event.getBlock();
        Block downBlock = snow.getRelative(BlockFace.DOWN);

        if (downBlock.getType().equals(Material.DIRT) && downBlock.getData() == 1 || specialBlock(downBlock)) {
            if (plugin.debug == 2) DirtPaths.LOGGER.info(plugin.getName() + ": Blocked Snow Form at " + event.getBlock().getLocation());
            event.setCancelled(true); // Cancel snow spread to this dirt
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.isCancelled()) return;
        Block blockClicked = event.getClickedBlock();
        Player player = event.getPlayer();
        if (plugin.playerHasPermission(player, "dirtpaths.dpath")) {
            if (blockClicked.getType().equals(Material.DIRT) && (event.getPlayer().getItemInHand().getTypeId() == plugin.wandId && plugin.wandId != 0)) {
                if (plugin.debug == 2) DirtPaths.LOGGER.info(plugin.getName() + ": Set Dirt/Grass spread prevention databit at " + blockClicked.getLocation());
                blockClicked.setData((byte) 1); // Set flag
            }
            else if (plugin.dirtToGrass && blockClicked.getType().equals(Material.GRASS) && (event.getPlayer().getItemInHand().getTypeId() == plugin.wandId && plugin.wandId != 0)) {
                if (plugin.debug == 2) DirtPaths.LOGGER.info(plugin.getName() + ": Changed to Dirt and set Dirt/Grass spread prevention databit at " + blockClicked.getLocation());
                blockClicked.setTypeIdAndData(3, (byte) 1, true); // change to dirt & set flag
            }
        }
    }

    private boolean specialBlock(Block block) {
        World world = block.getWorld();
        int typeId = block.getTypeId();
        int data = block.getData();
        GlobalBlocks gb = plugin.allWorlds.get(new GlobalBlocks(typeId, data));
        if (gb != null) return true;
        Map<DPSelectedWorlds, DPSelectedWorlds> blocks = plugin.selectWorlds.get(world.getName());
        if (blocks != null) {
            DPSelectedWorlds dpsw = new DPSelectedWorlds(world.getName(), typeId, (byte) data);
            if (blocks.containsKey(dpsw)) return true;
        }
        return false;
    }
}
