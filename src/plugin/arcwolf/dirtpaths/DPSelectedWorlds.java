package plugin.arcwolf.dirtpaths;

public class DPSelectedWorlds {

    public String name;
    public int itemId;
    public byte itemData;

    public DPSelectedWorlds(String name, int itemId, byte itemData) {
        this.name = name;
        this.itemId = itemId;
        this.itemData = itemData;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + itemData;
        result = prime * result + itemId;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        DPSelectedWorlds other = (DPSelectedWorlds) obj;
        if (itemData != other.itemData) return false;
        if (itemId != other.itemId) return false;
        if (name == null) {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        return true;
    }
}
