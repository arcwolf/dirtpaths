package plugin.arcwolf.dirtpaths;

import java.util.Hashtable;

import org.bukkit.entity.Player;

public class DirtPathSettings {
    @SuppressWarnings("rawtypes")
    static Hashtable playerSettings = new Hashtable();
    //command used
    public String command = "";
    
    @SuppressWarnings("unchecked")
    public static DirtPathSettings getSettings(Player player){
        DirtPathSettings settings = (DirtPathSettings) playerSettings.get(player.getName());
        if (settings == null) {
            playerSettings.put(player.getName(), new DirtPathSettings());
            settings = (DirtPathSettings) playerSettings.get(player.getName());
        }
        return (settings);
    }
}
