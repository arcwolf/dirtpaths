package plugin.arcwolf.dirtpaths;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.nijikokun.bukkit.Permissions.Permissions;

import de.bananaco.bpermissions.api.ApiLayer;
import de.bananaco.bpermissions.api.CalculableType;

import org.anjocaido.groupmanager.GroupManager;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class DirtPaths extends JavaPlugin {

    private GroupManager groupManager;
    private net.milkbowl.vault.permission.Permission vaultPerms;
    private Permissions permissionsPlugin;
    private PermissionsEx permissionsExPlugin;
    private de.bananaco.bpermissions.imp.Permissions bPermissions;

    private boolean permissionsEr = false;
    private boolean permissionsSet = false;
    private Server server;
    private String pluginName;

    public int wandId;
    public boolean dirtToGrass = false;
    public int debug = 0;
    public Map<GlobalBlocks, GlobalBlocks> allWorlds = new HashMap<GlobalBlocks, GlobalBlocks>();
    public Map<String, Map<DPSelectedWorlds, DPSelectedWorlds>> selectWorlds = new HashMap<String, Map<DPSelectedWorlds, DPSelectedWorlds>>();

    public static final Logger LOGGER = Logger.getLogger("Minecraft.DirtPaths");

    public void onEnable() {
        server = this.getServer();
        PluginDescriptionFile pdfFile = getDescription();
        pluginName = pdfFile.getName();
        PluginManager pm = getServer().getPluginManager();
        getPermissionsPlugin();

        wandId = getConfig().getInt("wandId", 0);
        debug = getConfig().getInt("debug", 0);
        dirtToGrass = getConfig().getBoolean("dirtToGrass", false);
        loadAllWorlds(getConfig().getString("blocks"));
        loadSelectedWorlds();

        pm.registerEvents(new DirtPathsBlockListener(this), this);
        getConfig().options().copyDefaults(true);
        saveConfig();

        if (debug == 3) {
            LOGGER.info(pluginName + ": loaded following blocks for all worlds");
            for(GlobalBlocks i : allWorlds.keySet()) {
                LOGGER.info("Blocks = " + i.getBlockId() + ":" + i.getBlockData());
            }
            LOGGER.info(pluginName + ": loaded following blocks for selected worlds");
            for(Entry<String, Map<DPSelectedWorlds, DPSelectedWorlds>> worlds : selectWorlds.entrySet()) {
                for(DPSelectedWorlds dpsw : worlds.getValue().values()) {
                    LOGGER.info("World = " + dpsw.name + " " + dpsw.itemId + ":" + dpsw.itemData);
                }
            }
        }
        LOGGER.info(pluginName + " version " + pdfFile.getVersion() + " is enabled!");
    }

    public void loadSelectedWorlds() {
        ConfigurationSection worldsSec = getConfig().getConfigurationSection("worlds");
        if (worldsSec == null) {
            saveConfig();
            worldsSec = getConfig().getConfigurationSection("worlds");
        }
        Map<String, Object> worlds = worldsSec.getValues(false);
        for(String key : worlds.keySet()) {
            List<String> idList = worldsSec.getStringList(key);
            Map<DPSelectedWorlds, DPSelectedWorlds> world = new HashMap<DPSelectedWorlds, DPSelectedWorlds>();
            for(String id : idList) {
                ItemStack is = parseMaterialID(id);
                if (is == null) {
                    if (debug == 3) {
                        System.out.println("String for block Id was " + id);
                        LOGGER.warning(pluginName + ": Could not parse block ID in config!");
                    }
                    continue;
                }
                DPSelectedWorlds dpsw = new DPSelectedWorlds(key, is.getTypeId(), is.getData().getData());
                world.put(dpsw, dpsw);
            }
            selectWorlds.put(key, world);
        }
    }

    public void loadAllWorlds(String blocks) {
        if (blocks == null) return;
        String[] split = blocks.split(",");
        for(String block : split) {
            ItemStack is = parseMaterialID(block);
            if (is == null) {
                if (debug == 3) {
                    System.out.println("String for block Id was " + block);
                    LOGGER.warning(pluginName + ": Could not parse block ID in config!");
                }
                continue;
            }
            GlobalBlocks gb = new GlobalBlocks(is.getTypeId(), is.getData().getData());
            allWorlds.put(gb, gb);
        }
    }

    public void onDisable() {
        PluginDescriptionFile pdfFile = getDescription();
        LOGGER.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
    }

    public boolean playerHasPermission(Player player, String command) {
        getPermissionsPlugin();
        if (vaultPerms != null) {
            if (debug == 1) {
                String pName = player.getName();
                String gName = vaultPerms.getPrimaryGroup(player);
                Boolean permissions = vaultPerms.has(player, command);
                LOGGER.info("Vault permissions, group for '" + pName + "' = " + gName);
                LOGGER.info("Permission for " + command + " is " + permissions);
            }
            return vaultPerms.has(player, command) || player.isOp() || player.hasPermission(command);
        }
        else if (groupManager != null) {
            if (debug == 1) {
                String pName = player.getName();
                String gName = groupManager.getWorldsHolder().getWorldData(player.getWorld().getName()).getPermissionsHandler().getGroup(player.getName());
                Boolean permissions = groupManager.getWorldsHolder().getWorldPermissions(player).has(player, command);
                LOGGER.info("group for '" + pName + "' = " + gName);
                LOGGER.info("Permission for " + command + " is " + permissions);
                LOGGER.info("");
                LOGGER.info("permissions available to '" + pName + "' = " + groupManager.getWorldsHolder().getWorldData(player.getWorld().getName()).getGroup(gName).getPermissionList());
            }
            return groupManager.getWorldsHolder().getWorldPermissions(player).has(player, command) || player.isOp() || player.hasPermission(command);
        }
        else if (permissionsPlugin != null) {
            if (debug == 1) {
                String pName = player.getName();
                String wName = player.getWorld().getName();
                String gName = Permissions.Security.getGroup(wName, pName);
                Boolean permissions = Permissions.Security.permission(player, command);
                LOGGER.info("Niji permissions, group for '" + pName + "' = " + gName);
                LOGGER.info("Permission for " + command + " is " + permissions);
            }
            return (Permissions.Security.permission(player, command)) || player.isOp() || player.hasPermission(command);
        }
        else if (permissionsExPlugin != null) {
            if (debug == 1) {
                String pName = player.getName();
                String wName = player.getWorld().getName();
                String[] gNameA = PermissionsEx.getUser(player).getGroupsNames(wName);
                StringBuffer gName = new StringBuffer();
                for(String groups : gNameA) {
                    gName.append(groups + " ");
                }
                Boolean permissions = PermissionsEx.getPermissionManager().has(player, command);
                LOGGER.info("PermissionsEx permissions, group for '" + pName + "' = " + gName.toString());
                LOGGER.info("Permission for " + command + " is " + permissions);
            }
            return (PermissionsEx.getPermissionManager().has(player, command)) || player.isOp() || player.hasPermission(command);
        }
        else if (bPermissions != null) {
            if (debug == 1) {
                String pName = player.getName();
                String wName = player.getWorld().getName();
                String[] gNameA = ApiLayer.getGroups(wName, CalculableType.USER, pName);
                StringBuffer gName = new StringBuffer();
                for(String groups : gNameA) {
                    gName.append(groups + " ");
                }
                Boolean permissions = bPermissions.has(player, command);
                LOGGER.info("bPermissions, group for '" + pName + "' = " + gName);
                LOGGER.info("bPermission for " + command + " is " + permissions);
            }
            return bPermissions.has(player, command) || player.isOp() || player.hasPermission(command);
        }
        else if (server.getPluginManager().getPlugin("PermissionsBukkit") != null && player.hasPermission(command)) {
            if (debug == 1) {
                LOGGER.info("Bukkit Permissions " + command + " " + player.hasPermission(command));
            }
            return true;
        }
        else if (permissionsEr && (player.isOp() || player.hasPermission(command))) {
            if (debug == 1) {
                LOGGER.info("Unknown permissions plugin " + command + " " + player.hasPermission(command));
            }
            return true;
        }
        else {
            if (debug == 1 && permissionsEr == true) {
                LOGGER.info("Unknown permissions plugin " + command + " " + player.hasPermission(command));
            }
            return false || player.isOp() || player.hasPermission(command);
        }
    }

    // permissions plugin enabled test
    private void getPermissionsPlugin() {
        if (server.getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
            if (!permissionsSet) {
                LOGGER.info(pluginName + ": Vault detected, permissions enabled...");
                permissionsSet = true;
            }
            vaultPerms = rsp.getProvider();
        }
        else if (server.getPluginManager().getPlugin("GroupManager") != null) {
            Plugin p = server.getPluginManager().getPlugin("GroupManager");
            if (!permissionsSet) {
                LOGGER.info(pluginName + ": GroupManager detected, permissions enabled...");
                permissionsSet = true;
            }
            groupManager = (GroupManager) p;
        }
        else if (server.getPluginManager().getPlugin("Permissions") != null) {
            Plugin p = server.getPluginManager().getPlugin("Permissions");
            if (!permissionsSet) {
                LOGGER.info(pluginName + ": Permissions detected, permissions enabled...");
                permissionsSet = true;
            }
            permissionsPlugin = (Permissions) p;
        }
        else if (server.getPluginManager().getPlugin("PermissionsBukkit") != null) {
            if (!permissionsSet) {
                LOGGER.info(pluginName + ": Bukkit permissions detected, permissions enabled...");
                permissionsSet = true;
            }
        }
        else if (server.getPluginManager().getPlugin("PermissionsEx") != null) {
            Plugin p = server.getPluginManager().getPlugin("PermissionsEx");
            if (!permissionsSet) {
                LOGGER.info(pluginName + ": PermissionsEx detected, permissions enabled...");
                permissionsSet = true;
            }
            permissionsExPlugin = (PermissionsEx) p;
        }
        else if (server.getPluginManager().getPlugin("bPermissions") != null) {
            Plugin p = server.getPluginManager().getPlugin("bPermissions");
            if (!permissionsSet) {
                LOGGER.info(pluginName + ": bPermissions detected, permissions enabled...");
                permissionsSet = true;
            }
            bPermissions = (de.bananaco.bpermissions.imp.Permissions) p;
        }
        else {
            if (!permissionsEr) {
                LOGGER.info(pluginName + ": Unknown permissions detected, Using Generic Permissions...");
                permissionsEr = true;
            }
        }
    }

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] split) {
        Player player = null;
        if ((sender instanceof Player)) {
            player = (Player) sender;
            String cmdname = cmd.getName().toLowerCase();
            if (cmdname.equals("dpath") && playerHasPermission(player, "dirtpaths.dpath"))
                if (!DirtPathSettings.getSettings(player).command.equals("dpath")) {
                    player.sendMessage(ChatColor.AQUA + "Dirt Path Creation" + ChatColor.GREEN + " Enabled." + ChatColor.WHITE + " To quit type: /dpath");
                    DirtPathSettings.getSettings(player).command = "dpath";
                }
                else {
                    player.sendMessage(ChatColor.AQUA + "Dirt Path Creation" + ChatColor.RED + " Disabled.");
                    DirtPathSettings.getSettings(player).command = "";
                }
            else {
                player.sendMessage(ChatColor.RED + "You dont have permission for Dirt Path Creation");
            }
        }
        else {
            sender.sendMessage("Not a console command");
            return true;
        }
        return true;
    }

    private ItemStack parseMaterialID(String str) {
        if (str != null) {
            if (str.contains(":")) {
                String[] parts = str.split(":");
                String sid = parts[0];
                String sdam = parts[1];
                if (isNumeric(sid) && isNumeric(sdam)) {
                    int id = Integer.parseInt(sid);
                    short dam = Short.parseShort(sdam);
                    Material mat = Material.getMaterial(id);
                    if (mat != null) {
                        if (dam == 0) {
                            return new ItemStack(mat, 1);
                        }
                        else {
                            return new ItemStack(mat, 1, dam);
                        }
                    }
                }
                else if (!isNumeric(sid) && isNumeric(sdam)) {
                    Material mat = Material.getMaterial(sid.toUpperCase());
                    if (mat != null) {
                        short dam = Short.parseShort(sdam);
                        if (mat != null) {
                            if (dam == 0) {
                                return new ItemStack(mat, 1);
                            }
                            else {
                                return new ItemStack(mat, 1, dam);
                            }
                        }
                    }
                }
            }
            else if (isNumeric(str)) {
                Material mat = Material.getMaterial(Integer.parseInt(str));
                if (mat != null) { return new ItemStack(mat, 1); }
            }
            else {
                Material mat = Material.getMaterial(str.toUpperCase());
                if (mat != null) { return new ItemStack(mat, 1); }
            }
        }
        return null;
    }

    private boolean isNumeric(String str) {
        if (str.equalsIgnoreCase("")) { return false; }
        if (str.contains(",")) { return false; }
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }
}
