Dirt Paths v1.4

Dirt Paths allows a user to type in a command and place dirt that grass will not spread to and snow wont form on. Thereby allowing the user to create dirt paths that will persist in the game world indefinitely.

Also, Dirt Paths allows an admin to set up a wand item to allow a user to click dirt and have that dirt not allow grass to spread or snow to form. The wand can also be set up to turn grass into dirt which will not allow grass or snow to form.

Command:

	dpath 
	
	Description: Type this command and then start placing dirt. Type the command again to turn off dirt path placement. All previously placed dirt will remain dirt while new placed dirt will allow grass to grow on it and snow to form on it.
	             
	Permissions(Groupmanager, bukkit permissions, ops) supported:
	
	dirtpaths.dpath
	dirtpaths.*
	
	Both permissions do the same thing and allow a user to use the command to place dirt that grass wont grow on and snow wont form on.

     
Config:

    wandId: 0 (default) disabled
        recommended 280 (stick)
        
    grassToDirt: false (default) wand wont turn grass into dirt path dirt
    
    world:
        list of worlds to check for snow forming
            list of blocks to test for snow forming on, can be numeric, numeric and datavalue, proper name, or proper name and datavalue. Proper names can be obtained from the craftbukkit Java Doc's Site
            NOTE: Number ID's separated by colon's for data values require single quotes, Proper block names do not. 
            
    blocks:
        a comma separated list of blocks to prevent snow forming on for all worlds on a server.
            Can be numeric, numeric and datavalue, proper name, or proper name and datavalue. Proper names can be obtained from the craftbukkit Java Doc's Site
            NOTE: If only one value is used it must be surrounded by single quotes. If more then one value is used single quotes can be dropped and values are separated by comma's 
            
    debug: 0 (default) disabled
        1 shows permissions info
        2 shows info about grass and snow prevention
        3 shows info about config file loading

    EXAMPLE config.yml:

    wandId: 0
    dirtToGrass: false
      worlds:
        exampleworld:
          - 1
          - 2
          - cobblestone
          - wool:2
        exampleworld2:
          - '35:3'
          - dirt:1
        world:
          - '3:1'
          - 159
        blocks: '3:1'
        debug: 0

The mechanics of this plugin are as such:

When a user types in the dpath command or uses the wand a variable is set that tells the plugin that if a dirt block is placed it needs to set the dirts data value to 1. Normally dirt has a data value of 0. If the a grass spread event is fired by bukkit then this plugin checks to see if the dirt that is being spread to has a data value of 1. If it does have a 1 for its data value the event it canceled. This is the same for when snow fires its form event.

The result is any dirt that has a data value of 1 will not have grass spread to it. The only incompatibility this might have is if another plugin uses data values and dirt. This plugin only listens for dirt with a value of 1. So if the dirt does not have a 1 then grass will spread to it.